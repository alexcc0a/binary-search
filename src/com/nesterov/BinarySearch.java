package com.nesterov;

import java.util.Scanner;

public class BinarySearch {

    public static void main(String[] args) {
        int[] arr = {2, 3, 5, 8, 15, 23, 56, 78, 90};
        Scanner sc = new Scanner(System.in);
        int x = sc.nextInt();
        int left = 0; // Объявление и инициализация переменной left со значением 0.
        int right = arr.length - 1; // Объявление и инициализация переменной right со
        // значением длины массива arr минус 1.

        while (left <= right) { // Начало цикла while, который будет выполняться до тех пор,
            // пока значение переменной left меньше или равно значению переменной right.
            int mid = (left + right) / 2; // Вычисление среднего индекса массива arr и сохранение его в переменную mid.

            if (arr[mid] == x) { // Проверка, равно ли значение элемента массива arr с индексом mid значению переменной x.
                System.out.println("Номер я чейки с числом " + x + " равен " + mid);
                return;
            } else if (arr[mid] < x) { // Проверка, меньше ли значение элемента массива arr с индексом mid значения переменной x.
                left = mid + 1; // Присвоение переменной left значения mid плюс 1.
            } else {
                right = mid - 1; // Присвоение переменной right значения mid минус 1.
            }
        }
        System.out.println("Число " + x + " не найдено в массиве");
    }
}
